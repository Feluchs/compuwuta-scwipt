# Compuwuta Scwipt
*Compuwuta Scwipt* is a programming language that is inspired by OWO-speech and furry-*slang* (if you wanna call it that). It doesn't has a real purpose and is meant as a joke. *(The language is still in it's design phase. Expect some of the basics to change)*

## Basic Syntax
The overall syntax is similar to high-level languages like C#, Kotlin and JavaScript. It doesn't follow a certain language and just uwu-fies it but rather combines different concepts/syntax features. For example the type declaration is similar to C#, while the use of a statement terminator is optional like in Kotlin. To get an overview over all basics see the list below:

- Variables can be declared both implicitly and explicitly. See section [Variables](#variables) for more information
- Logical and
- The use of a statement terminator `:§` is optional
- Comments can be written by using `:3` for one line and `:< [...] >:` for multi line ones
- The program can be started through a main method/function. If no is found/specified the scripts starts at the first line

## Features
*Compuwuta Scwipt* aims to support all common operations available in other (esoteric) programming languages like variables, logical operations, loops, condition checks and so on and so forth. Since the language is still in development pleas see the list below to check which features are already implemented and which not.

- [ ] [variables](#variables)
- [ ] [mathematical operations](#math-ops)
- [ ] [logical operations](#logic-ops)
- [ ] [conditions (if/when/switch-case)](#conditions)
- [ ] [user-IO (aka. print/input)](#user-io)
- [ ] [loops (for/while/do)](#loops)
- [ ] [functions](#functions)
- [ ] [file/network read/write](#file-networking)

# <a name="variables"></a>
### Variables
Variables are implicitly declared by default. To declare one the keywords `war` and `wal` are used. The difference between those two will be explained further down.

```
war hewow = "Hewow :3"
wal nuwber = 123
```

In this example the variable `hewow` is of type `maw` and `nuwber` a variable of type `snoot`. The type doesn't need to be explicitly declared when a variable is initialized with a value. It gets figured out on the fly by the interpret/compiler.

If wanted, the type can be explicitly specified though:
```
maw hewow = "Hewow :3"
snoot nuwber = 123
```

Once a variable is defined, it can not be redefined as a different type. If you should try it anyways it will throw an error. Speaking of which, here are the types available:

#### Types
- Numeric
  - bean (byte)*
  - paw (short)*
  - snoot (int)*
  - tail (long)*
  - fuzzy (float)
  - fluffy (double)
- Characters
  - teef (char)
  - maw (string)
- bowl (boolean)
- awray (array)
*\*only whole numbers*

If you need to convert a variable from one type to another that can be done by using the `to{type}()` function or `({type})` (for numbers only) when setting the value.

Excamples:
```
war somePaw = 5
maw someSpeak = "The number is "

bean some bean = (bean) somePaw
someSpeak = someSpeak + somePaw.toMaw()
```

#### Specialties for Maws
Maws can be chained together by using `+` or `+=` when adding to an already existing maw. Other types will also be automatically converted to a maw when included.

```
maw someMaw = "This will" + " be chained "
someMaw += 2 + "gether" :3 'someMaw += 2' would not work

bark(someMaw) :3 will output "This will be chained 2gether"
```

# <a name="math-ops"></a>
### Mathematical operations
The mathematical operations are nothing special. If you have some sort of experience with programming, it should be familiar to you.

 Operation | Symbol
-|-
Addition | +
Subtraction | -
Multiplication | *
Division | /
Module | %
Powe

On top of those braces (these right here →`()`) are also supported. The order of operator is from the most inner braces to the most outer, Multiplication & Division before Addition & Subtraction and otherwise from left to right.

Excamples:
```
war value1 = 3 + 4        :3 will set value1 to 7
war value2 = 8 / 3        :3 will set value2 to 2.666666667
paw value3 = 8 / 3        :3 will set value3 to 2 (since paw)
war value4 = 8 % 3        :3 will set value4 to 2
war value5 = (3 - 2) * 3  :3 will set value5 to 3 (3-2=1 & 1x3=3)
```

# <a name="logic-ops"></a>
### Logical operations
> Not yet implemented

# <a name="conditions"></a>
### Conditions
#### OwO|OvO/-w-|-v-
The `OwO` (or optionally `OvO` for you avians out there) keyword is this languages version of the well known `if` keyword and functions like you would expect. It checks if a given logical condition is `yis` or not.

Example:
```
OwO ([condition]) {
  :3 some code
}
```

The `-w-` (or optionally `OvO`) keyword is the `else` equivalent and it's usage is also. The code written after it gets executed when the logical conditions checked in the `OwO` returns `nay`

Example:
```
OwO ([condition]) {
  :3 some code
} -w- {
  :3 some other code
}
```
# <a name="user-io"></a>
### User-IO
> Not yet implemented

# <a name="loops"></a>
### Loops
> Not yet implemented

# <a name="functions"></a>
### Functions
> Not yet implemented

# <a name="file-networking"></a>
### File/Networking
> Not yet implemented

#### Footnote
In case you are wondering why the README.md file isn't written in OWO-speech: I'm not a fur. I would go insane. And yes I know that it's odd (espieciall) for a non-fur to do a project like that. And yes you read right, I am not a furry. And yes I know how my avatar looks like. WHO DO YOU THINK PUT IT THERE?

Just let Khajiit be themself in peace, yes?
